import board
import pwmio
import time

pwm = pwmio.PWMOut(board.A9, frequency=1000)

loop = 0
while True:
    loop = loop + 1
    a = input()
    print("Hello, CircuitPython! {}, {}".format(a, loop))
    
    if a == "1":
        pwm.duty_cycle = 0xffff
    elif a == "0":
        pwm.duty_cycle = 0x0000
    else:
        print("wrong value")


