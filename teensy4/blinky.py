import board
import digitalio
import time

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

loop = 0
while True:
    loop = loop+1
    print("Hello, CircuitPython! {}".format(loop))
    led.value = True
    time.sleep(0.1)
    led.value = False
    time.sleep(0.1)

    