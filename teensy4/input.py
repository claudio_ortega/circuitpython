import board
import digitalio
import time

led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

loop = 0
while True:
    loop = loop + 1
    a = input()
    print("Hello, CircuitPython! {}, {}".format(a, loop))
    
    if a == "1":
        led.value = True
    elif a == "0":
        led.value = False
    else:
        print("wrong value")




    